#include<stdio.h>
#include<stdlib.h>
#include<time.h>

/* Game of Life
 *
 * Cellular Automata Rules
 *
 * Any live cell with fewer than two live neighbours dies, as if by underpopulation.
 * Any live cell with two or three live neighbours lives on to the next generation.
 * Any live cell with more than three live neighbours dies, as if by overpopulation.
 * Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
 *
 * Condensed Rules
 *
 * Any live cell with two or three live neighbours survives.
 * Any dead cell with three live neighbours becomes a live cell.
 * All other live cells die in the next generation. Similarly, all other dead cells stay dead.
 *
 * EXPERIMENTS
 * Compile the program using
 * gcc -fopenmp -O0 GameOfLife.c
 * ./a.out
 * Then try
 * gcc -fopenmp -O3 GameOfLife.c
 * ./a.out
 *
 * REFERENCE
 * https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
*/

int main(){
        int n=1000;
	int *A;
	int *B;
	int steps=100;
	int iplus;
	int iminus;
	int jplus;
	int jminus;
	clock_t start;
	clock_t stop;
        double diff;

	A=(int*)malloc(n*n*sizeof(int));
        B=(int*)malloc(n*n*sizeof(int));

	// choose 0 for dead and 1 for alive
	for(int i=0;i<n*n;i++) A[i]=((rand())%2);
	start = clock();
	for(int t=0;t<steps;t+=2){
		for(int j=0;j<n;j++){
			jplus=(j+1)%n;
			jminus=(j+n-1)%n;	
			for(int i=0;i<n;i++){
				iplus=(i+1)%n;
				iminus=(i+n-1)%n;
				int count=0;
				count=count+A[j*n+iplus]+A[j*n+iminus]+
					A[jplus*n+iplus]+A[jplus*n+i]+A[jplus*n+iminus]+
					A[jminus*n+iplus]+A[jminus*n+i]+A[jminus*n+iminus];
				if(count==3 && A[j*n+i]==0){
					B[j*n+i]=1;
				}else if(A[j*n+i]==1 && (count>=2) && (count<=3)){
					B[j*n+i]=1;
				}else{
					B[j*n+i]=0;
				}
			}
		}
		for(int j=0;j<n;j++){
                        jplus=(j+1)%n;
                        jminus=(j+n-1)%n;
                        for(int i=0;i<n;i++){
                                iplus=(i+1)%n;
                                iminus=(i+n-1)%n;
                                int count=0;
                                count=count+B[j*n+iplus]+B[j*n+iminus]+
                                        B[jplus*n+iplus]+B[jplus*n+i]+B[jplus*n+iminus]+
                                        B[jminus*n+iplus]+B[jminus*n+i]+B[jminus*n+iminus];
                                if(count==3 && B[j*n+i]==0){
                                        A[j*n+i]=1;
                                }else if(B[j*n+i]==1 && (count>=2) && (count<=3)){
                                        A[j*n+i]=1;
                                }else{
                                        A[j*n+i]=0;
                                }
                        }
		}
	}
	stop = clock();
	diff = (double)(stop - start) / CLOCKS_PER_SEC;
	printf("The program took %f seconds.\n",diff);
	free(A);
	free(B);
	return 0;
}
