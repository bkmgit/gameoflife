#include<stdio.h>
#include<stdlib.h>
#include "mpi.h"
/* Game of Life
 *
 * Cellular Automata Rules
 *
 * Any live cell with fewer than two live neighbours dies, as if by underpopulation.
 * Any live cell with two or three live neighbours lives on to the next generation.
 * Any live cell with more than three live neighbours dies, as if by overpopulation.
 * Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
 *
 * Condensed Rules
 *
 * Any live cell with two or three live neighbours survives.
 * Any dead cell with three live neighbours becomes a live cell.
 * All other live cells die in the next generation. Similarly, all other dead cells stay dead.
 *
 * EXPERIMENTS
 * Compile the program using
 * mpicc -O0 GameOfLife.c
 * mpirun -np 1 ./a.out
 * mpirun -np 2 ./a.out
 * Then try
 * mpicc -O3 GameOfLife.c
 * mpirun -np 1 ./a.out
 * mpirun -np 2 ./a.out
 *
 * REFERENCE
 * https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
 * https://computing.llnl.gov/tutorials/mpi
 * https://mpitutorial.com/tutorials/mpi-send-and-receive/
*/

int main(int argc, char *argv[]){
        int n=1000;
	int *A;
	int *B;
	int *send;
	int *recv;
	int steps=10;
	int iplus;
	int iminus;
	int jplus;
	int jminus;
	int numranks;
	int rank;
	int myjstart;
	int nextjstart;
	int ret;
	int t;
	int i;
	int j;
	int count;
	int source;
	int dest;
	int tag;
	int startcolumn;
	double start;
	double stop;
	double diff;
	MPI_Status status;

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numranks);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	// Allocate memory
	A = (int *) malloc((n+5)*(n+5)*sizeof(int));
	B = (int *) malloc((n+5)*(n+5)*sizeof(int));
	send = (int *) malloc((n+5)*sizeof(int));
	recv = (int *) malloc((n+5)*sizeof(int));
	// Use slab decomposition
        myjstart=(rank*n)/numranks;
        nextjstart=((rank+1)*n)/numranks;
	// choose 0 for dead and 1 for alive
	for(j=myjstart;j<nextjstart;j++){
		for(i=0;i<n;i++){
			A[j*n+i]=((rand())%2);
		}
	}
	ret = MPI_Barrier(MPI_COMM_WORLD);
	start = MPI_Wtime();
	for(t=0;t<steps;t+=2){
	        // Exchange data if there is more than one rank
                if(numranks>1){
                        // Send data to the right
                        for(i=0;i<n;i++) send[i]=A[((nextjstart-1+n)%n)*n+i];
                        // Even ranks send first
                        if(rank%2==0){
				dest=(rank+1)%numranks;
				tag=1;
                                MPI_Send(send,n,MPI_INT,dest,tag,MPI_COMM_WORLD);
                        }else{
				source = (rank-1+numranks)%numranks;
				tag=1;
                                MPI_Recv(recv,n,MPI_INT,source,tag,MPI_COMM_WORLD,&status);
                        }
			ret = MPI_Barrier(MPI_COMM_WORLD);
                        // Odd ranks send second
                        if(rank%2==1){
				dest = (rank+1)%numranks;
				tag = 2;
                                MPI_Send(send,n,MPI_INT,dest,tag,MPI_COMM_WORLD);
                        }else{
				source = (rank-1+numranks)%numranks;
				tag = 2;
                                MPI_Recv(recv,n,MPI_INT,source,tag,MPI_COMM_WORLD,&status);
                        }
			ret = MPI_Barrier(MPI_COMM_WORLD);
                        // Copy data into array
			startcolumn = ((myjstart-1+n)%n);
                        for(i=0;i<n;i++) A[startcolumn*n + i]= recv[i];
                        // Send data to the left
                        for(i=0;i<n;i++) send[i]=A[(myjstart)*n+i];
                        // Even ranks send first
                        if(rank%2==0){
				dest = (rank-1+numranks)%numranks;
				tag = 3;
                                MPI_Send(send,n,MPI_INT,dest,tag,MPI_COMM_WORLD);
                        }else{
				source = (rank+1)%numranks;
				tag = 3;
                                MPI_Recv(recv,n,MPI_INT,source,tag,MPI_COMM_WORLD,&status);
                        }
                        // Odd ranks send second
                        if(rank%2==1){
				dest = (rank-1+numranks)%numranks;
				tag = 4;
                                MPI_Send(send,n,MPI_INT,dest,tag,MPI_COMM_WORLD);
                        }else{
				source = (rank+1)%numranks;
				tag = 4;
                                MPI_Recv(recv,n,MPI_INT,source,tag,MPI_COMM_WORLD,&status);
                        }
                        // Copy data into array
                        for(i=0;i<n;i++) A[((nextjstart+n)%n)*n+i]=recv[i];
                }
		for(j=myjstart;j<nextjstart;j++){
			jplus=(j+1)%n;
			jminus=(j+n-1)%n;	
			for(i=0;i<n;i++){
				iplus=(i+1)%n;
				iminus=(i+n-1)%n;
				count=0;
				count=count+A[j*n+iplus]+A[j*n+iminus]+
					A[jplus*n+iplus]+A[jplus*n+i]+A[jplus*n+iminus]+
					A[jminus*n+iplus]+A[jminus*n+i]+A[jminus*n+iminus];
				if(count==3 && A[j*n+i]==0){
					B[j*n+i]=1;
				}else if(A[j*n+i]==1 && (count>=2) && (count<=3)){
					B[j*n+i]=1;
				}else{
					B[j*n+i]=0;
				}
			}
		}
		// Exchange data if there is more than one rank
		if(numranks>1){
                        // Send data to the right
                        for(i=0;i<n;i++) send[i]=B[((nextjstart-1+n)%n)*n+i];
                        // Even ranks send first
                        if(rank%2==0){
                                dest=(rank+1)%numranks;
                                tag=1;
                                MPI_Send(send,1,MPI_INT,dest,tag,MPI_COMM_WORLD);
                        }else{
                                source = (rank-1+numranks)%numranks;
                                tag=1;
                                MPI_Recv(recv,n,MPI_INT,source,tag,MPI_COMM_WORLD,&status);
                        }
                        ret = MPI_Barrier(MPI_COMM_WORLD);
                        // Odd ranks send second
                        if(rank%2==1){
                                dest = (rank+1)%numranks;
                                tag = 2;
                                MPI_Send(send,n,MPI_INT,dest,tag,MPI_COMM_WORLD);
                        }else{
                                source = (rank-1+numranks)%numranks;
                                tag = 2;
                                MPI_Recv(recv,n,MPI_INT,source,tag,MPI_COMM_WORLD,&status);
                        }
                        ret = MPI_Barrier(MPI_COMM_WORLD);
                        // Copy data into array
                        startcolumn = ((myjstart-1+n)%n);
                        for(i=0;i<n;i++) B[startcolumn*n + i]= recv[i];
                        // Send data to the left
                        for(i=0;i<n;i++) send[i]=B[(myjstart)*n+i];
                        // Even ranks send first
                        if(rank%2==0){
                                dest = (rank-1+numranks)%numranks;
                                tag = 3;
                                MPI_Send(send,n,MPI_INT,dest,tag,MPI_COMM_WORLD);
                        }else{
                                source = (rank+1)%numranks;
                                tag = 3;
                                MPI_Recv(recv,n,MPI_INT,source,tag,MPI_COMM_WORLD,&status);
                        }
                        // Odd ranks send second
                        if(rank%2==1){
                                dest = (rank-1+numranks)%numranks;
                                tag = 4;
                                MPI_Send(send,n,MPI_INT,dest,tag,MPI_COMM_WORLD);
                        }else{
                                source = (rank+1)%numranks;
                                tag = 4;
                                MPI_Recv(recv,n,MPI_INT,source,tag,MPI_COMM_WORLD,&status);
                        }
                        // Copy data into array
                        for(i=0;i<n;i++) B[((nextjstart+n)%n)*n+i]=recv[i];
		}
		// Odd iteration
		for(j=myjstart;j<nextjstart;j++){
                        jplus=(j+1)%n;
                        jminus=(j+n-1)%n;
                        for(i=0;i<n;i++){
                                iplus=(i+1)%n;
                                iminus=(i+n-1)%n;
                                count=0;
                                count=count+B[j*n+iplus]+B[j*n+iminus]+
                                        B[jplus*n+iplus]+B[jplus*n+i]+B[jplus*n+iminus]+
                                        B[jminus*n+iplus]+B[jminus*n+i]+B[jminus*n+iminus];
                                if(count==3 && B[j*n+i]==0){
                                        A[j*n+i]=1;
                                }else if(B[j*n+i]==1 && (count>=2) && (count<=3)){
                                        A[j*n+i]=1;
                                }else{
                                        A[j*n+i]=0;
                                }
                        }
		}
	}
	ret = MPI_Barrier(MPI_COMM_WORLD);
	stop = MPI_Wtime();
	diff = stop - start;
	if(rank==0) printf("The program took %f seconds.\n",diff);
	free(A);
	free(B);
	free(send);
	free(recv);
	MPI_Finalize();
	return 0;
}
