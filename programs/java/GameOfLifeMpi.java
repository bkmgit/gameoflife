/**
 * Game of Life
 *
 * Cellular Automata Rules
 *
 * Any live cell with fewer than two live neighbours dies, as if by underpopulation.
 * Any live cell with two or three live neighbours lives on to the next generation.
 * Any live cell with more than three live neighbours dies, as if by overpopulation.
 * Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
 *
 * Condensed Rules
 *
 * Any live cell with two or three live neighbours survives.
 * Any dead cell with three live neighbours becomes a live cell.
 * All other live cells die in the next generation. Similarly, all other dead cells stay dead.
 *
 * EXPERIMENTS
 * Compile the program:
 * export CLASSPATH=/mnt/data/shared/software/openmpi-4.0.5/build/ompi/mpi/java/java/mpi.jar:$CLASSPATH
 * mpijavac GameOfLifeMpi.java
 * 
 * Run the program:
 * mpirun -np 1 java GameOfLifeMpi
 * mpirun -np 2 java GameOfLifeMpi
 *
 * REFERENCE
 * https://gitlab.com/bkmgit/gameoflife/-/blob/master/programs/c/MPI/GameOfLife.c
 * https://gitlab.com/bkmgit/gameoflife/-/blob/master/programs/java/GameOfLife.java
 * https://blogs.cisco.com/performance/java-bindings-for-open-mpi
 * https://www.openmp.org/wp-content/uploads/OpenMP-API-Specification-5-1.pdf
 * 
 * 
 */
import mpi.*;
/**
 * 
 */
public class GameOfLifeMpi {
    public static void main(String[] args) throws MPIException {

        MPI.Init(args) ;
        int rank= MPI.COMM_WORLD.getRank();
        int numranks = MPI.COMM_WORLD.getSize();

        int n = 1000;
        int[] A = new int[n*n];
        int[] B = new int[n*n];
        int steps = 100;
        int i,j,t,count,startcolumn;
        int iplus;
        int iminus;
        int jplus;
        int jminus;
        double start;
        double stop;
        double diff;
        int source;
        int dest;
        int tag;
        @SuppressWarnings("unused")
        Status status;

        int[] send = new int[n];
        int[] recv = new int[n];
        
        // Use slab decomposition
        int myjstart=(rank*n)/numranks; 
        int nextjstart=((rank+1)*n)/numranks;
        @SuppressWarnings("unused")
        int ret;
        
        // choose 0 for dead and 1 for alive
        for (i = 0; i < n*n; i++) {
                A[i] = ((int) (Math.random() * 32768)) % 2;
        }
        // ret = MPI_Barrier(MPI_COMM_WORLD);
        MPI.COMM_WORLD.barrier();
        start = MPI.wtime();
        for (t = 0; t < steps; t += 2) {
            if (numranks>1){
                ////////////////////
                // Send data to the right
                for(i=0;i<n;i++) 
                    send[i]=A[((nextjstart-1+n)%n)*n+i];
                // Even ranks send first
                if(rank%2==0){
                    dest=(rank+1)%numranks;
                    tag=1;
                    MPI.COMM_WORLD.send(send, n, MPI.INT,dest, tag);
                }else{
                    source = (rank-1+numranks)%numranks;
                    tag=1;
                    status=MPI.COMM_WORLD.recv(recv, n, MPI.INT, source, tag);
                }
                // ret = MPI_Barrier(MPI_COMM_WORLD);
                MPI.COMM_WORLD.barrier();
                // Odd ranks send second
                if(rank%2==1){
                    dest = (rank+1)%numranks;
                    tag = 2;
                    MPI.COMM_WORLD.send(send,n,MPI.INT,dest,tag);
                }else{
                    source = (rank-1+numranks)%numranks;
                    tag = 2;
                    status=MPI.COMM_WORLD.recv(recv,n,MPI.INT,source,tag);
                }
                // ret = MPI_Barrier(MPI_COMM_WORLD);
                MPI.COMM_WORLD.barrier();
                // Copy data into array
                startcolumn = ((myjstart-1+n)%n);
                for(i=0;i<n;i++) 
                    A[startcolumn*n + i]= recv[i];

                ///////////////////////
                // Send data to the left
                for(i=0;i<n;i++) 
                    send[i]=A[(myjstart)*n+i];
                
                // Even ranks send first
                if(rank%2==0){
                    dest = (rank-1+numranks)%numranks;
                    tag = 3;
                    MPI.COMM_WORLD.send(send,n,MPI.INT,dest,tag);
                }else{
                    source = (rank+1)%numranks;
                    tag = 3;
                    status=MPI.COMM_WORLD.recv(recv,n,MPI.INT,source,tag);
                }
                // Odd ranks send second
                if(rank%2==1){
                    dest = (rank-1+numranks)%numranks;
                    tag = 4;
                    MPI.COMM_WORLD.send(send,n,MPI.INT,dest,tag);
                }else{
                    source = (rank+1)%numranks;
                    tag = 4;
                    status=MPI.COMM_WORLD.recv(recv,n,MPI.INT,source,tag);
                }
                // Copy data into array
                for(i=0;i<n;i++) 
                    A[((nextjstart+n)%n)*n+i]=recv[i];
            }
            ///////////////////////
            ///////////////////////

            for(j=myjstart;j<nextjstart;j++){
                jplus=(j+1)%n;
                jminus=(j+n-1)%n;
                for(i=0;i<n;i++){
                    iplus=(i+1)%n;
                    iminus=(i+n-1)%n;
                    count=A[j*n+iplus]+A[j*n+iminus]+A[jplus*n+iplus]+A[jplus*n+i]+A[jplus*n+iminus]+A[jminus*n+iplus]+A[jminus*n+i]+A[jminus*n+iminus];
                    if(count==3 && A[j*n+i]==0){
                        B[j*n+i]=1;
                    }else if(A[j*n+i]==1 && (count>=2) && (count<=3)){
                        B[j*n+i]=1;
                    }else{
                        B[j*n+i]=0;
                    }
                }
            }    
            ////////////////////
            ////////////////////

            // Exchange data if there is more than one rank
		    if(numranks>1){
                ///////////////////////
                // Send data to the right
                for(i=0;i<n;i++) 
                    send[i]=B[((nextjstart-1+n)%n)*n+i];
                // Even ranks send first
                if(rank%2==0){
                    dest=(rank+1)%numranks;
                    tag=1;
                    MPI.COMM_WORLD.send(send,n,MPI.INT,dest,tag);
                }else{
                    source = (rank-1+numranks)%numranks;
                    tag=1;
                    status=MPI.COMM_WORLD.recv(recv,n,MPI.INT,source,tag);
                }
                // ret = MPI_Barrier(MPI_COMM_WORLD);
                MPI.COMM_WORLD.barrier();
                // Odd ranks send second
                if(rank%2==1){
                    dest = (rank+1)%numranks;
                    tag = 2;
                    MPI.COMM_WORLD.send(send,n,MPI.INT,dest,tag);
                }else{
                    source = (rank-1+numranks)%numranks;
                    tag = 2;
                    status=MPI.COMM_WORLD.recv(recv,n,MPI.INT,source,tag);
                }
                // ret = MPI_Barrier(MPI_COMM_WORLD);
                MPI.COMM_WORLD.barrier();
                // Copy data into array
                startcolumn = ((myjstart-1+n)%n);
                for(i=0;i<n;i++) 
                    B[startcolumn*n + i]= recv[i];

                ///////////////////////////////
                // Send data to the left
                for(i=0;i<n;i++) 
                    send[i]=B[(myjstart)*n+i];
                // Even ranks send first
                if(rank%2==0){
                    dest = (rank-1+numranks)%numranks;
                    tag = 3;
                    MPI.COMM_WORLD.send(send,n,MPI.INT,dest,tag);
                }else{
                    source = (rank+1)%numranks;
                    tag = 3;
                    status=MPI.COMM_WORLD.recv(recv,n,MPI.INT,source,tag);
                }
                // Odd ranks send second
                if(rank%2==1){
                    dest = (rank-1+numranks)%numranks;
                    tag = 4;
                    MPI.COMM_WORLD.send(send,n,MPI.INT,dest,tag);
                }else{
                    source = (rank+1)%numranks;
                    tag = 4;
                    status=MPI.COMM_WORLD.recv(recv,n,MPI.INT,source,tag);
                }
                // Copy data into array
                for(i=0;i<n;i++) 
                    B[((nextjstart+n)%n)*n+i]=recv[i];
            }
            // Odd iteration
            for(j=myjstart;j<nextjstart;j++){
                jplus=(j+1)%n;
                jminus=(j+n-1)%n;
                for(i=0;i<n;i++){
                    iplus=(i+1)%n;
                    iminus=(i+n-1)%n;
                    count=B[j*n+iplus]+B[j*n+iminus]+B[jplus*n+iplus]+B[jplus*n+i]+B[jplus*n+iminus]+B[jminus*n+iplus]+B[jminus*n+i]+B[jminus*n+iminus];
                    if(count==3 && B[j*n+i]==0){
                            A[j*n+i]=1;
                    }else if(B[j*n+i]==1 && (count>=2) && (count<=3)){
                            A[j*n+i]=1;
                    }else{
                            A[j*n+i]=0;
                    }
                }
            }
        }
        stop = MPI.wtime();
        diff = (stop - start); 
        if(rank==0) System.out.printf("The program took " + diff + " seconds.\n");
        MPI.Finalize();
    }
}
