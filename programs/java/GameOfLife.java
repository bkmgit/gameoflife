public class GameOfLife {
    public static void main(String[] args) {

        int n = 1000;
        int[] A = new int[n*n];
        int[] B = new int[n*n];
        int steps = 100;
        int iplus;
        int iminus;
        int jplus;
        int jminus;
        long start;
        long stop;
        long diff;

//      choose 0 for dead and 1 for alive
        for (int i = 0; i < n*n; i++) {
                A[i] = ((int) (Math.random() * 32768)) % 2;
        }
        start = System.currentTimeMillis();
        for (int t = 0; t < steps; t += 2) {
            for (int j = 0; j < n; j++) {
                jplus = (j + 1) % n;
                jminus = (j + n - 1) % n;
                for (int i = 0; i < n; i++) {
                    iplus = (i + 1) % n;
                    iminus = (i + n - 1) % n;
                    int count = 0;
                    count = count + A[j*n+iplus] + A[j*n+iminus] +
                            A[jplus*n+iplus] + A[jplus*n+i] + A[jplus*n+iminus] +
                            A[jminus*n+iplus] + A[jminus*n+i] + A[jminus*n+iminus];
                    if (count == 3 && A[j*n+i] == 0) {
                        B[j*n+i] = 1;
                    } else if (A[j*n+i] == 1 && (count >= 2) && (count <= 3)) {
                        B[j*n+i] = 1;
                    } else {
                        B[j*n+i] = 0;
                    }
                }
            }
            for (int j = 0; j < n; j++) {
                jplus = (j + 1) % n;
                jminus = (j + n - 1) % n;
                for (int i = 0; i < n; i++) {
                    iplus = (i + 1) % n;
                    iminus = (i + n - 1) % n;
                    int count = 0;
                    count = count + B[j*n+iplus] + B[j*n+iminus] +
                            B[jplus*n+iplus] + B[jplus*n+i] + B[jplus*n+iminus] +
                            B[jminus*n+iplus] + B[jminus*n+i] + B[jminus*n+iminus];
                    if (count == 3 && B[j*n+i] == 0) {
                        A[j*n+i] = 1;
                    } else if (B[j*n+i] == 1 && (count >= 2) && (count <= 3)) {
                        A[j*n+i] = 1;
                    } else {
                        A[j*n+i] = 0;
                    }
                }
            }
        }
        stop = System.currentTimeMillis();
        diff = (stop - start) 
        System.out.printf("The program took " + diff + " miliseconds.");


    }
}
